# POO IFC 2021 

> Repositório para estudos de programação orientada a objetos em C++ e da biblioteca SDL. Esses códigos fazem parte dos meus estudos enquanto participo da disciplina de Programação Orientada a Objetos do segundo semestre do curso de Bacharelado em Ciências da Computação do Instituto Federal Catarinense Campus Blumenau.   

---

## Aviso

Os códigos desse repositório podem não funcionar como esperado ou sofrer alterações drásticas. O objetivo é guiar meus estudos, portanto a melhoria será contínua. 
