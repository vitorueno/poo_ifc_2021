#include "vetor.h"

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
    cout << "Teste de construtores: \n";
    Vetor v(10, 50);
    Vetor v2(0, 0, 30, 45);
    v.imprimir();
    v2.imprimir();

    cout << "\nTeste do método somar: \n";
    Vetor soma = v.somar(v2);
    soma.imprimir();

    cout << "\nTeste do método subtrair: \n";
    Vetor subtracao = v.subtrair(v2);
    subtracao.imprimir();

    cout << "\nTeste do método multiplicação por escalar: \n";
    Vetor multEscalar = v.multPorEscalar(10);
    multEscalar.imprimir();

    cout << "\nTeste do método módulo: \n";
    double modulo = v.modulo();
    cout << modulo << "\n";

    cout << "\nTeste do método produto escalar: \n";
    double pEscalar = v.prodEscalar(v2);
    cout << pEscalar << "\n";
    return 0;
}
