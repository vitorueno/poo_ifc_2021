#ifndef BLA_VETOR_H
#define BlA_VETOR_H

#include <iostream>
#include <cmath>
using namespace std;

/**
 *    Declaração da classe vetor
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 **/
class Vetor
{
public:
    double x;
    double y;

    Vetor(double x, double y);
    Vetor(double x1, double y1, double x2, double y2);
    ~Vetor();

    void imprimir();
    Vetor somar(Vetor v2);
    Vetor subtrair(Vetor v2);
    Vetor multPorEscalar(double escalar);
    double modulo();
    double prodEscalar(Vetor v2);
};

#endif