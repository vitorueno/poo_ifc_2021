#include "vetor.h"

/**
 * Implementação do construtor do vetor recebendo apenas um x e um y
 * \author: Vítor Augusto Ueno Otto
 * \date: 31 de outubro de 2021
 * \param x: valor do x de um ponto
 * \param y: valor do y de um ponto
 *  **/
Vetor::Vetor(double x, double y)
{
    Vetor::x = x;
    Vetor::y = y;
}

/**
* Implementação do construtor do vetor recebendo um x e y inicial e um x e y final
* \author: Vítor Augusto Ueno Otto
* \date: 31 de outubro de 2021
* \param x1: valor do x do primeiro ponto
* \param x1: valor do x do primeiro ponto
* \param y2: valor do y do segundo ponto
* \param y2: valor do y do segundo ponto
*  **/

Vetor::Vetor(double x1, double y1, double x2, double y2)
{
    double xFinal = x2 - x1;
    double yFinal = y2 - y1;

    x = xFinal;
    y = yFinal;
}

Vetor::~Vetor() {}

/**
* Função que imprime informações uteis do vetor (x, y, modulo...) 
* \author: Vítor Augusto Ueno Otto
* \date: 31 de outubro de 2021
**/
void Vetor::imprimir()
{
    cout << "(" << x;
    cout << ", " << y << ") ";
    cout << "modulo: " << this->modulo() << "\n";
}

/**
* Implementação da função de soma que retorna um novo vetor. 
* \author: Vítor Augusto Ueno Otto
* \date: 31 de outubro de 2021
* \param v2: o vetor que será somado ao vetor da classe 
**/
Vetor Vetor::somar(Vetor v2)
{
    Vetor tmp(x, y);
    tmp.x += v2.x;
    tmp.y += v2.y;

    return tmp;
}

/**
* Implementação da função de subtração que retorna um novo vetor. 
* \author: Vítor Augusto Ueno Otto
* \date: 31 de outubro de 2021
* \param v2: o vetor que será subtraído ao vetor da classe 
**/
Vetor Vetor::subtrair(Vetor v2)
{
    Vetor tmp(x, y);
    tmp.x -= v2.x;
    tmp.y -= v2.y;

    return tmp;
}

/**
* Implementação da função de multiplicação por escalar que retorna um novo vetor
* \author: Vítor Augusto Ueno Otto
* \date: 31 de outubro de 2021
* \param escalar: um número (escalar) que multiplicará cada componente do vetor
**/
Vetor Vetor::multPorEscalar(double escalar)
{
    Vetor tmp(x, y);
    tmp.x *= escalar;
    tmp.y *= escalar;

    return tmp;
}

/**
* Implementação da função que retorna o módulo do vetor
* \author: Vítor Augusto Ueno Otto
* \date: 31 de outubro de 2021
**/
double Vetor::modulo()
{
    return sqrt(pow(x, 2) + pow(y, 2));
}

/**
* Implementação da função que retorna o produto escalar dese com outro vetor
* \author: Vítor Augusto Ueno Otto
* \date: 31 de outubro de 2021
* \param v2: outro vetor
**/
double Vetor::prodEscalar(Vetor v2)
{
    return Vetor::x * v2.x + Vetor::y * v2.y;
}