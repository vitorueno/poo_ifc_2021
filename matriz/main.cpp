#include "matriz.h"

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
    cout << "Teste do construtor e método de inicializar matriz:\n";
    Matriz matrix(3, 3);
    matrix.imprimir();

    cout << "\nTeste do método que verifica se é matriz quadrada: \n";
    string s = matrix.ehMatrizQuadrada() ? "é matriz quadrada" : "não é matriz quadrada";
    cout << s << "\n";

    return 0;
}
