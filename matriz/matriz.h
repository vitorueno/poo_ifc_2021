#ifndef BLA_MATRIZ_H
#define BLA_MATRIZ_H

#include <iostream>
using namespace std;

/**
 *    Declaração da classe matriz
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 **/
class Matriz
{
public:
    double **m;
    int linhas;
    int colunas;

    Matriz(int linhas, int colunas);
    ~Matriz();

    void init(int linhas, int colunas);
    void finalize();
    bool ehMatrizQuadrada();
    void imprimir();
};

#endif