#include "matriz.h"

/**
 *    implementação do construtor da matriz
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 *    \param linhas: quantidade de linhas da matriz
 *    \param colunas: quantidade de colunas da matriz
 **/
Matriz::Matriz(int linhas, int colunas)
{
    Matriz::linhas = linhas;
    Matriz::colunas = colunas;
    init(linhas, colunas);
}

/**
 *    implementação do destrutor da matriz
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 **/
Matriz::~Matriz() { finalize(); }

/**
 *    implementação do método de inicialização da matriz
 *    executado no construtor
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 *    \param linhas: quantidade de linhas da matriz
 *    \param colunas: quantidade de colunas da matriz
 **/
void Matriz::init(int linhas, int colunas)
{
    m = new double *[linhas];

    for (int i = 0; i < linhas; i++)
    {
        m[i] = new double[colunas];
        for (int j = 0; j < colunas; j++)
        {
            m[i][j] = 0;
        }
    }
}

/**
 *    implementação do método de destruição da matriz
 *    executado no construtor e pode ser chamada sozinha
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 **/
void Matriz::finalize()
{
    for (int i = 0; i < linhas; i++)
    {
        delete[] m[i];
    }

    delete[] m;
    m = nullptr;
}

/**
 *    implementação do método de impressão da matriz
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 **/
void Matriz::imprimir()
{
    for (int i = 0; i < linhas; i++)
    {
        cout << "| ";
        for (int j = 0; j < colunas; j++)
        {
            cout << m[i][j] << " ";
        }
        cout << "|\n";
    }
}

/**
 *    implementação do método que retorna se a matriz é quadrada ou não
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 31 de outubro de 2021
 **/
bool Matriz::ehMatrizQuadrada()
{
    return linhas == colunas;
}