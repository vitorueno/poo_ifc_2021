#include "polinomio.h"

#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
    Polinomio teste(3);
    teste.imprimir();

    teste.definirMonomio(3, 15);
    teste.definirMonomio(2, -2);
    teste.definirMonomio(1, 1);
    teste.definirMonomio(0, -2);

    teste.imprimir();

    cout << teste.avaliar(2) << "\n";
    return 0;
}
