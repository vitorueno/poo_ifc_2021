#ifndef BLA_POLINOMIO_H
#define BLA_POLINOMIO_H

#include <iostream>
#include <cmath>
using namespace std;

/**
 *    Declaração da classe Polinomio
 *    baseado nos códigos do professor Eder
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 1º de novembro de 2021
 **/
class Polinomio
{
public:
    int grau;
    double *p;

    Polinomio(int grau);
    ~Polinomio();

    void definirMonomio(int grau, double monomio);
    void imprimir();

    double avaliar(double x);
};

#endif