#include "polinomio.h"

/**
 *    Declaração do método construtor do polinomio
 *    baseado nos códigos do professor Eder
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 1º de novembro de 2021
 *    \param grau: grau do polinomio (maior expoente) 
 **/
Polinomio::Polinomio(int grau)
{
    Polinomio::grau = grau;
    p = new double[grau + 1];
}

/**
 *    Declaração do destrutor do polinomio
 *    baseado nos códigos do professor Eder
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 1º de novembro de 2021
 **/
Polinomio::~Polinomio()
{
    delete[] p;
    p = nullptr;
}

/**
 *    Declaração do método de impressão do polinomio
 *    baseado nos códigos do professor Eder
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 1º de novembro de 2021
 **/
void Polinomio::imprimir()
{
    for (int i = grau; i >= 0; i--)
    {
        if (p[i] != 0)
        {
            if (i == 0)
            {
                cout << p[i] << "\n";
            }
            else if (i == 1)
            {
                cout << p[i] << "x"
                     << " + ";
            }
            else
                cout << p[i] << "x^" << i << " + ";
        }
    }
}

/**
 *    Declaração do método que define os coeficientes de um 
 *    monomio do polinomio
 *    baseado nos códigos do professor Eder
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 1º de novembro de 2021
 *    \param grau: indica qual o monomio se quer definir o coeficiente
 *    \param monomio: indica o valor do coeficiente
 **/
void Polinomio::definirMonomio(int grau, double monomio)
{
    p[grau] = monomio;
}

/**
 *    Declaração do método que retorna o valor do polinomio em um dado ponto
 *    baseado nos códigos do professor Eder
 *    \author: Vítor Augusto Ueno Otto
 *    \date: 1º de novembro de 2021
 *    \param x: um número que será substituído na função 
 **/
double Polinomio::avaliar(double x)
{
    double resultado = 0;
    for (int i = 0; i < grau + 1; i++)
    {
        resultado += p[i] * pow(x, i);
    }
    return resultado;
}