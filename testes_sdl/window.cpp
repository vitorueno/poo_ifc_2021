#include <iostream>
using namespace std;

#include "definicoes/window.h"
#include "definicoes/ponto.h" 

Window::Window(int width, int height): width{width}, height{height} {  
    window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, 
        SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN );
    
    if (window == NULL) {
        cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
        exit(1);
    }

    surface = SDL_GetWindowSurface(window);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

Window::~Window() {
    SDL_DestroyWindow(window);
    cout << "Finalizando a Window" << "\n";
}


void Window::drawLine(int x1, int y1, int x2, int y2) {
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0xFF);		
    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
}
    
void Window::drawLine(Ponto p1, Ponto p2) {
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0xFF);
    drawLine(p1.x, p1.y, p2.x, p2.y);
}

void Window::clear() {
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderClear(renderer);
}

void Window::update() {
    SDL_RenderPresent(renderer);
}

