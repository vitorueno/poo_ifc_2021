#ifndef BLA_WINDOW_H
#define BLA_WINDOW_H

#include <SDL.h>
#include "ponto.h"

class Window {
    protected:
        int width; 
        int height;
        
        SDL_Window * window;
        SDL_Surface * surface;
        SDL_Renderer * renderer = NULL;

    public:
        Window(int width, int height);
        ~Window();

        void drawLine(int x1, int y1, int x2, int y2);
        void drawLine(Ponto p1, Ponto p2);
        void clear();
        void update();
};

#endif