#ifndef BLA_SDL_H
#define BLA_SDL_H

#include <SDL.h>

class SDL {
    public: 
        SDL();

        bool init();
        bool finalize();
};



#endif