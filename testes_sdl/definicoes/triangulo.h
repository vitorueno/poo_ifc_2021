#ifndef BLA_TRIANGULO_H
#define BLA_TRIANGULO_H

#include "window.h"
#include "ponto.h" 

class Triangulo {
    public:
        Ponto p[3];

        Triangulo(Ponto p[]);
        void draw(Window &w);
};

#endif