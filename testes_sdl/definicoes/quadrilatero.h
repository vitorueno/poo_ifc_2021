#ifndef BLA_QUADRILATERO_H
#define BLA_QUADRILATERO_H

#include "window.h"
#include "ponto.h" 

class Quadrilatero {
    public:
        Ponto p[4];

        Quadrilatero(Ponto p[]);
        void draw(Window &w);
};

#endif