#include "definicoes/sdl.h"
#include <iostream>

using namespace std; 

SDL::SDL() {}

bool SDL::init() {
    if (SDL_Init( SDL_INIT_VIDEO ) < 0) {
        cerr << "Erro ao inicializar SDL! Erro do SDL: " << SDL_GetError() << "\n";
        exit(1);
        return false;
    } 

    return true; 
}

bool SDL::finalize() {
    SDL_Quit();
    return true;
}