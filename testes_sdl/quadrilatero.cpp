#include "definicoes/quadrilatero.h"


Quadrilatero::Quadrilatero(Ponto p[]) {
    for (int i = 0; i < 4; i++) {
        Quadrilatero::p[i] = p[i];
    }
}


void Quadrilatero::draw(Window &w) {
    w.drawLine(p[0], p[1]);
    w.drawLine(p[1], p[2]);
    w.drawLine(p[2], p[3]);
    w.drawLine(p[3], p[0]);
}