#include "definicoes/triangulo.h"


Triangulo::Triangulo(Ponto p[]) {
    for (int i = 0; i < 3; i++) {
        Triangulo::p[i] = p[i];
    }
}


void Triangulo::draw(Window &w) {
    w.drawLine(p[0], p[1]);
    w.drawLine(p[1], p[2]);
    w.drawLine(p[2], p[0]);
}