#include <iostream>
using namespace std; 

#include "definicoes/sdl.h"
#include "definicoes/ponto.h"
#include "definicoes/window.h"
#include "definicoes/quadrilatero.h"
#include "definicoes/triangulo.h"

int main(int argc, char *argv[]) {
    SDL sdl;
    sdl.init();    
	
    Window w(800, 600);

    // pontos para as formas geometricas
    Ponto pq[] = { Ponto(100, 100), Ponto(100, 150), 
                Ponto(150,150), Ponto(150,100) };

    Ponto pt[] = { Ponto(200, 200), Ponto(200, 300),
                     Ponto(300,300) };

    // formas geometricas
    Quadrilatero q(pq);
    Triangulo t(pt);
    
    SDL_Event e;
    
    bool quit = false;
    while (!quit) {
        while(SDL_PollEvent(&e) != 0) {
            if(e.type == SDL_QUIT) {
                quit = true;
            }
        }

        w.clear();
        
        w.drawLine(0, 0, 100, 50);
        
        q.draw(w);
        t.draw(w);

        w.update();
    }
    
    sdl.finalize();
    return 0;
}